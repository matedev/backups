#!/bin/bash
rsync -arRP /home/mate/ /media/HDD/backups/rpi/
rsync -arRP /etc/ /media/HDD/backups/rpi/
tar czf /media/HDD/backups/rpi/backup_`date +\%Y_\%m_\%d`.tgz /media/HDD/backups/rpi/etc  /media/HDD/backups/rpi/home
rm -rf /media/HDD/backups/rpi/etc
rm -rf /media/HDD/backups/rpi/home
find /media/HDD/backups/rpi/ -name "backup*_01.tgz" -type f -exec mv {} /media/HDD/backups/rpi/month/ \;
find /media/HDD/backups/rpi/backup* -type f -mtime +2 | xargs rm -f;
