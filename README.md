Create backup for my files. 
 
------------------------------ 
 
Bash script to make daily backup of my files from SD card in RPi to HDD connected to USB. 
 
The script creates backup from last three days. 
Additionally, each backup from the first day of a month is stored in the "month" folder separately. 

------------------------------
The script run from cron every day at 3:05am, and store error (if exists) in rpi.log file.

Script /home/mate/scripts/backups/rpi_backup_to_router.sh send backup files to my router with USB Disc on every Sunday at 4:01am. 

To activate autostart script you need to edit crontab `sudo crontab -e`. (Remember to leave the last line as empty)
```
4 3 * * * echo "Error from: `date +\%Y_\%m_\%d`:" >> /home/mate/scripts/backups/rpi.log
5 3 * * * bash /home/mate/scripts/backups/rpi_backup.sh  2>> /home/mate/scripts/backups/rpi.log
1 4 * * 0 bash /home/mate/scripts/backups/rpi_backup_to_router.sh 2>> /home/mate/scripts/backups/router.log
```

_The paths should be adapted to your needs_
